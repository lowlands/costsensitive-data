#!/bin/bash

## wants comma-separated labels as input; once the *cm file is generated, call plot.sh
mkdir -p data/confusions/jan13/

python scripts/confusion_matrix_from_CSV.py data/annotations/twitter/annotation-all.txt.csv > data/confusions/jan13/twitter-initial+group.cm

