'''
plot hinton diagram of a confusion matrix over N tags of the format
where value1,...,valueN are raw confusion counts.

<tag1>    <tag2>    ...    <tagN>
value1    value2    ...    valueN
...
value1    value2    ...    valueN

and write it to a file.

Usage: plot-hinton.py <input file> <output name>

 
 @author: bplank, dirkh Nov 21, 2013


changelog:
- Jan 13, 2014: use raw count as input, ignore "FLAG" tag

'''

import sys
import numpy as np
import matplotlib
matplotlib.use('Agg')# Must be before importing matplotlib.pyplot or pylab to run plots remotely (e.g. on zen)
import matplotlib.pyplot as plt

## hinton diagram, taken from http://matplotlib.org/examples/specialty_plots/hinton_demo.html
def hinton(matrix, max_weight=None, ax=None):
    """Draw Hinton diagram for visualizing a weight matrix."""
    ax = ax if ax is not None else plt.gca()

    if not max_weight:
        max_weight = 2**np.ceil(np.log(np.abs(matrix).max())/np.log(2))

    ax.patch.set_facecolor('white')
    ax.set_aspect('equal', 'box')
    ax.xaxis.set_major_locator(plt.NullLocator())
    ax.yaxis.set_major_locator(plt.NullLocator())

    for (x,y),w in np.ndenumerate(matrix):
        #color = 'gray' if w > 0 else 'black'
        color = "gray"
        size = np.sqrt(np.abs(w))
        rect = plt.Rectangle([x - size / 2, y - size / 2], size, size,
                             facecolor=color, edgecolor=color)
        ax.add_patch(rect)

    ax.autoscale_view()
    ax.invert_yaxis()

def main():

    input_file = sys.argv[1]
    output = sys.argv[2]
    data = open(input_file).readlines()
    matrix=[]
    labels=[]
    i=0
    idx_flag=-1
    for line in data:
        if i==0: 
            labels = line.strip().split("\t")
            if "FLAG" in labels:
                idx_flag=labels.index("FLAG")
                labels.remove("FLAG")
        else:
            raw_counts = np.array([float(x) for x in line.strip().split("\t")[0:]])
            #matrix.append(raw_counts/sum(raw_counts))
            matrix.append(raw_counts)
            
            
        i+=1

    npmatrix = np.array(matrix)

    #check if we need to remove FLAG info
    if idx_flag > 0:
        npmatrix = np.delete(npmatrix,idx_flag,0) # delete row idx_flag
        npmatrix = np.delete(npmatrix,idx_flag,1) # " column "

    
    #normalize by taking count of confusion A,B and B,A by total count of label A and B
    
    totals = npmatrix.sum(axis=1) # row-wise totals 
    rows,cols = npmatrix.shape
    
    normalized_matrix = np.zeros(npmatrix.shape)

    for i in xrange(rows):
        for j in xrange(cols): 
            value = (npmatrix[i,j] + npmatrix[j,i]) / (totals[i] + totals[j])
            normalized_matrix[i,j] = value
            normalized_matrix[j,i] = value

    print("normalized matrix:")    
    print(normalized_matrix.shape)
    hinton(normalized_matrix)
    plt.xticks(np.arange(len(labels)),labels,rotation=45)
    plt.yticks(np.arange(len(labels)),labels)
    #plt.show()
    plt.savefig("%s.png" % output, bbox_inches="tight")

if __name__=="__main__":
    main()
