# -*- coding: utf-8 -*-
"""
get confusion matrix from crowdsourced CSV file

Created on Wed Nov 20 15:06:21 2013

@author: dirkhovy
"""
import sys
import numpy as N
from collections import defaultdict

csv_file = sys.argv[1]
counts = defaultdict(lambda: defaultdict(int))
tagset = set()

for line in open(csv_file, 'rU'):
    line = line.strip()
    if line:
        tokens = [x for x in line.split(",") if x!='']
        for i, token1 in enumerate(tokens[:-1]):
            tagset.add(token1)
            for token2 in tokens[i+1:]:
                counts[token1][token2] += 1
                # if we are off the diagonal, add to both
                # if we are on the diaginal, just count the agreement once
                if token1 != token2:
                    counts[token2][token1] += 1

tags = sorted(list(tagset))
tag2int = dict( ((tag, i) for (i,tag) in enumerate(tags)) )
T = len(tags)

W = N.zeros((T,T))

for tag1 in tags:
    tag1_id = tag2int[tag1]
    for tag2 in tags:
        tag2_id = tag2int[tag2]            
        W[tag1_id, tag2_id] = counts[tag1][tag2]

#for t in xrange(T):
#    W[t] /= W[t].sum()
    
print "\t".join(tags)
for tag in tags:
    print "\t".join(map(str, W[tag2int[tag]]))
